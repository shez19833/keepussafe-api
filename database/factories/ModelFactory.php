<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Address::class, function (Faker\Generator $faker) {
    return [
        'line_1' => $faker->streetName,
        'line_2' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->city,
        'postcode' => $faker->postcode,
        'country_id' => random_int(1, 10),

        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
    ];
});

$factory->define(App\Message::class, function (Faker\Generator $faker) {
    return [
        'tracker_id' => function()
        {
            return \App\Tracker::inRandomOrder()->first()->id;
        },
        'text' => $faker->sentence,
    ];
});

$factory->define(App\Tracker::class, function (Faker\Generator $faker) {
    return [
        'user_id' => function()
        {
            return \App\User::first()->id;
        },
        'location_id' => factory(\App\Address::class)->create()->id,
        'address_id' => factory(\App\Address::class)->create()->id,
        'start_time' => $faker->dateTime(),
        'end_time' => $faker->dateTime('+1 hour'),
        'check_in_frequency' => $faker->randomElement([10, 15, 20, 25, 30]),
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName(),
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'password' => app('hash')->make('test1234'),
        'email_verified' => random_int(0, 1),
        'secret_word' => $faker->word,
        'secret_response' => $faker->word,
        'timezone' => $faker->timezone,
        'api_token' => \App\ApiToken::createApiToken(),
    ];
});

$factory->state(App\User::class, 'verified', function() {
    return [
        'email_verified' => 1,
    ];
});


