<?php

use Illuminate\Database\Seeder;

class UsersFriendsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();

        $users->each(function($user)
        {
            $friends = factory(\App\User::class, random_int(1,3))->create();

            $user->friends()->sync($friends->pluck('id')->toArray());
        });
    }
}
