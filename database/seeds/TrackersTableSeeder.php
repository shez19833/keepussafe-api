<?php

use Illuminate\Database\Seeder;

class TrackersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::all(['id'])->each(function ($user)
        {
            factory(\App\Tracker::class)->create([
                'user_id' => $user->id,
            ]);
        });
    }
}
