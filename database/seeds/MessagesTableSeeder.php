<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Tracker::all(['id'])->each(function ($tracker)
        {
            factory(\App\Message::class, random_int(2, 5))->create([
                'tracker_id' => $tracker->id,
            ]);
        });
    }
}
