<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('plans', 'PlansController@index');


$router->post('register', 'UsersController@register');
$router->post('login', 'UsersController@login');

$router->get('trackers/status/{status}', 'UsersTrackersController@status');

$router->group(['middleware' => 'auth'], function () use ($router) {

    $router->group(['prefix' => 'users/me'], function () use ($router) {

        $router->get('/', 'UsersController@show');
        $router->post('/', 'UsersController@update');

        $router->get('friends', 'UsersFriendsController@index');
        $router->post('friends', 'UsersFriendsController@store');
        $router->delete('friends/{id}', 'UsersFriendsController@delete');

        $router->get('trackers', 'UsersTrackersController@index');
        $router->post('trackers', 'UsersTrackersController@store');

        $router->get('trackers/{id}', 'UsersTrackersController@show');
        $router->post('trackers/{id}', 'UsersTrackersController@update');

        $router->post('/subscriptions', 'SubscriptionController@create');
        $router->delete('/subscriptions', 'SubscriptionController@destroy');

    });

    // TODO Maybe use pusher
    $router->group(['prefix' => 'trackers/{id}'], function () use ($router) {
        $router->get('messages', 'TrackersMessagesController@index');
        $router->post('messages', 'TrackersMessagesController@store');
    });

});
