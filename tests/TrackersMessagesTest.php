<?php

use App\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TrackersMessagesTest extends TestCase
{
    use DatabaseTransactions;

    private function sanitiseItems($items)
    {
        $data = [];

        foreach ($items as $item)
            $data[] = $this->sanitiseItem($item);

        return $data;
    }

    private function sanitiseItem($item)
    {
        return [
            'text' => (string) $item->text,
            'timestamp' => (string) $item->created_at,
        ];
    }

    /**
     * @return void
     */
    public function test_i_can_get_all_messages_of_a_tracker()
    {
        $user = User::with('trackers.location', 'trackers.messages')->first();

        $tracker = $user->trackers->first();

        $this->actingAs($user)->json('GET', sprintf('trackers/%d/messages', $tracker->id));

        $this->seeJsonContains(['data' => $this->sanitiseItems($tracker->messages)]);
    }

    /**
     * @return void
     */
    public function test_i_can_post_a_message_for_a_tracker()
    {
        Event::fake();

        $user = User::with('trackers.location', 'trackers.messages')->first();

        $tracker = $user->trackers->first();

        $this->actingAs($user)->json('POST',
            sprintf('trackers/%d/messages', $tracker->id), ['text' => 'some text']);

        Event::assertNotDispatched(\App\Events\TrackerAlert::class);

        $this->seeInDatabase('messages', ['tracker_id' => $tracker->id, 'text' => 'some text']);
    }

    /**
     * @return void
     */
    public function test_i_can_detect_secret_word_in_a_message_posted_for_a_tracker()
    {
        Event::fake();

        $user = User::with('trackers.location', 'trackers.messages')->first();

        $text = 'some text ' . $user->secret_word . ' some text ';

        $tracker = $user->trackers->first();

        $this->actingAs($user)->json('POST',
            sprintf('trackers/%d/messages', $tracker->id), ['text' => $text]);

        Event::assertDispatched(\App\Events\TrackerAlert::class, function ($e) use ($tracker) {
            return $e->tracker->id === $tracker->id;
        });

        $this->assertContains("$user->secret_response", $this->response->getContent());
    }

//    /**
//     * @return void
//     */
//    public function test_i_can_get_a_trackers_of_a_user()
//    {
//        $user = User::with('trackers.location', 'trackers.address')->first();
//        $tracker = $user->trackers->first();
//
//        $this->actingAs($user)->json('GET', sprintf('users/me/trackers/%d', $tracker->id));
//
//        $this->seeJsonContains(['data' => $this->sanitiseItem($tracker)]);
//    }
//
//    /**
//     * @return void
//     */
//    public function test_i_can_create_a_tracker_for_a_user()
//    {
//        $user = User::first();
//        $tracker = factory(\App\Tracker::class)->make();
//
//        $data = [
//            'start_time' => (string) $tracker->start_time,
//            'end_time' => (string)  $tracker->end_time,
//            'check_in_frequency' => $tracker->check_in_frequency,
//            'latitude' => $tracker->location->latitude,
//            'longitude' => $tracker->location->longitude,
//        ];
//
//        $this->actingAs($user)->json('POST', sprintf('users/me/trackers'), $data);
//
//        unset($data['latitude'], $data['longitude']);
//
//        $data['user_id'] = $user->id;
//
//        $this->seeInDatabase('trackers', $data);
//    }
//
//    /**
//     * @return void
//     */
//    public function test_i_can_update_a_tracker_for_a_user()
//    {
//        $user = User::with('trackers')->first();
//        $tracker = $user->trackers->first();
//
//        $newInfo = factory(\App\Tracker::class)->make();
//
//        $data = [
//            'start_time' => (string) $newInfo->start_time,
//            'end_time' => (string)  $newInfo->end_time,
//            'check_in_frequency' => $newInfo->check_in_frequency,
//            'latitude' => $newInfo->location->latitude,
//            'longitude' => $newInfo->location->longitude,
//        ];
//
//        $this->actingAs($user)->json('POST',
//            sprintf('users/me/trackers/%d', $tracker->id), $data);
//
//        unset($data['latitude'], $data['longitude']);
//
//        $data['user_id'] = $user->id;
//        $data['id'] = $tracker->id;
//
//        $this->seeInDatabase('trackers', $data);
//    }
//
//    public function test_users_can_see_trackers_status()
//    {
//        $user = User::with('trackers')->first();
//        $tracker = $this->sanitiseItem($user->trackers->first());
//        unset($tracker['address']);
//
//        $this->json('GET', sprintf('trackers/status/%s', $tracker['share_id']))
//            ->seeJsonContains(['data' => $tracker]);
//    }
}
