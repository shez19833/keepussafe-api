<?php

use App\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UsersTest extends TestCase
{
    use DatabaseTransactions;

    private function sanitiseItem($item)
    {
        return [
            'id' => (int) $item->id,
            'first_name' => (string) $item->first_name,
            'last_name' => (string) $item->last_name,
            'email' => (string) $item->email,
            'secret_word' => (string) $item->secret_word,
            'secret_response' => (string)  $item->secret_response,
            'timezone' => (string)  $item->timezone,
        ];
    }

    /**
     * @return void
     */
    public function test_i_can_get_a_user()
    {
        $user = User::first();

        $this->actingAs($user)->json('GET', 'users/me');
        $this->seeJsonContains(['data' => $this->sanitiseItem($user)]);
    }

    /**
     * @return void
     */
    public function test_i_can_register_a_user()
    {
        $user = factory(User::class)->make();

        $this->json('POST', 'register', [
            'email' => $user->email,
            'password' => 'test1234',
        ]);

        $this->assertNotNull(User::whereEmail($user['email'])->first());
    }

    /**
     * @return void
     */
    public function test_i_can_login_a_user()
    {
        $user = factory(User::class)->create();

        $this->json('POST', 'login', [
            'email' => $user->email,
            'password' => 'test1234',
        ])->seeJsonContains(['data' => ['api_token' => $user->api_token]]);
    }

    /**
     * @return void
     */
    public function test_i_can_update_a_user()
    {
        $user = factory(User::class)->create();

        $update = [
            'first_name' => 'someone',
            'last_name' => 'else',
            'secret_word' => 'secret word',
            'secret_response' => 'secret response',
            'timezone' => 'timezone',

        ];

        $this->actingAs($user)->json('POST', 'users/me', $update);

        $user = User::find($user->id);

        $user = [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'secret_word' => $user->secret_word,
            'secret_response' => $user->secret_response,
            'timezone' => $user->timezone,
        ];

        $this->assertEquals($update, $user);
    }
}
