<?php

use App\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UsersTrackersTest extends TestCase
{
    use DatabaseTransactions;

    private function sanitiseItems($items)
    {
        $data = [];

        foreach ($items as $item)
            $data[] = $this->sanitiseItem($item);

        return $data;
    }

    private function sanitiseItem($item)
    {
        // TODO what if no address.. location will be
        return [
            'location' => (array) $this->sanitiseAddress($item->location),
//            'address' => $this->sanitiseAddress($item->address),
            'start_time' => (string) $item->start_time,
            'end_time' => (string) $item->end_time,
            'check_in_frequency' => (int) $item->check_in_frequency,
            'share_id' => (string) $item->share_id,
            'status' => (string) $item->status,
        ];
    }

    private function sanitiseAddress($item)
    {
        return [
            'line_1' => (string) $item->line_1,
            'line_2' => (string)  $item->line_2,
            'city' => (string) $item->city,
            'state' => (string) $item->state,
            'postcode' => (string) $item->postcode,
            'country_id' => (string) $item->country_id,

            'latitude' => (double) $item->latitude,
            'longitude' => (double) $item->longitude,
        ];
    }

    /**
     * @return void
     */
    public function test_i_can_get_all_trackers_of_a_user()
    {
        $user = User::with('trackers.location', 'trackers.address')->first();

        $this->actingAs($user)->json('GET', sprintf('users/me/trackers'));

        $this->seeJsonContains(['data' => $this->sanitiseItems($user->trackers)]);
    }

    /**
     * @return void
     */
    public function test_i_can_get_a_trackers_of_a_user()
    {
        $user = User::with('trackers.location', 'trackers.address')->first();
        $tracker = $user->trackers->first();

        $this->actingAs($user)->json('GET', sprintf('users/me/trackers/%d', $tracker->id));

        $this->seeJsonContains(['data' => $this->sanitiseItem($tracker)]);
    }

    /**
     * @return void
     */
    public function test_i_can_create_a_tracker_for_a_user()
    {
        $user = User::first();
        $tracker = factory(\App\Tracker::class)->make();

        $data = [
            'start_time' => (string) $tracker->start_time,
            'end_time' => (string)  $tracker->end_time,
            'check_in_frequency' => $tracker->check_in_frequency,
            'latitude' => $tracker->location->latitude,
            'longitude' => $tracker->location->longitude,
        ];

        $this->actingAs($user)->json('POST', sprintf('users/me/trackers'), $data);

        unset($data['latitude'], $data['longitude']);

        $data['user_id'] = $user->id;

        $this->seeInDatabase('trackers', $data);
    }

    /**
     * @return void
     */
    public function test_i_can_update_a_tracker_for_a_user()
    {
        $user = User::with('trackers')->first();
        $tracker = $user->trackers->first();

        $newInfo = factory(\App\Tracker::class)->make();

        $data = [
            'start_time' =>  (string) $newInfo->start_time,
            'end_time' =>  (string) $newInfo->end_time,
            'check_in_frequency' => (int) $newInfo->check_in_frequency,
            'latitude' =>  (double) $newInfo->location->latitude,
            'longitude' => (double) $newInfo->location->longitude,
        ];

        $this->actingAs($user)->json('POST',
            sprintf('users/me/trackers/%d', $tracker->id), $data);

        unset($data['latitude'], $data['longitude']);

        $data['user_id'] = $user->id;
        $data['id'] = $tracker->id;

        $this->seeInDatabase('trackers', $data);
    }

    public function test_users_can_see_trackers_status()
    {
        $user = User::with('trackers')->first();
        $tracker = $this->sanitiseItem($user->trackers->first());
        unset($tracker['address']);

        $this->json('GET', sprintf('trackers/status/%s', $tracker['share_id']))
            ->seeJsonContains(['data' => $tracker]);
    }
}
