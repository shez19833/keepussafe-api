<?php

use App\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class TrackerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @return void
     */
    public function test_tracker_status_comes_green_when_no_message_within_frequency()
    {
        $tracker = factory(\App\Tracker::class)->create();
        $tracker->check_in_frequency = '10';

        //9:51pm
        $tracker->start_time = \Carbon\Carbon::now()->subMinutes($tracker->check_in_frequency - 2);
        $tracker->save();

        $this->assertEquals(\App\Tracker::STATUS_GREEN, $tracker->status);
    }

    /**
     * @return void
     */
    public function test_tracker_status_comes_orange_when_no_message_and_outside_frequency()
    {
        $tracker = factory(\App\Tracker::class)->create();
        $tracker->check_in_frequency = '10';

        //9:51pm
        $tracker->start_time = \Carbon\Carbon::now()->subMinutes($tracker->check_in_frequency * 2);
        $tracker->save();

        $this->assertEquals(\App\Tracker::STATUS_ORANGE, $tracker->status);
    }

    /**
     * @return void
     */
    public function test_tracker_status_comes_red_when_no_message_and_outside_orange_frequency()
    {
        $tracker = factory(\App\Tracker::class)->create();
        $tracker->check_in_frequency = '10';

        //9:51pm
        $tracker->start_time = \Carbon\Carbon::now()->subMinutes($tracker->check_in_frequency * 3);
        $tracker->save();

        $this->assertEquals(\App\Tracker::STATUS_RED, $tracker->status);
    }

    /**
     * @return void
     */
    public function test_tracker_status_comes_green_when_message_within_frequency()
    {
        $tracker = factory(\App\Tracker::class)->create();
        $tracker->check_in_frequency = '10';

        //9:30pm
        $tracker->start_time = \Carbon\Carbon::now()->subMinutes($tracker->check_in_frequency);
        $tracker->save();

        $message = factory(\App\Message::class)->create([
           'tracker_id' => $tracker,
        ]);

        //9:38
        $message->created_at = $tracker->start_time->addMinutes($tracker->check_in_frequency - 2);
        $message->save();

        $this->assertEquals(\App\Tracker::STATUS_GREEN, $tracker->status);
    }

    /**
     * @return void
     */
    public function test_tracker_status_comes_orange_when_message_outside_of_a_frequency()
    {
        $tracker = factory(\App\Tracker::class)->create();
        $tracker->check_in_frequency = '10';

        //9:03pm
        $tracker->start_time = \Carbon\Carbon::now()->subMinutes($tracker->check_in_frequency * 3);
        $tracker->save();

        $message = factory(\App\Message::class)->create([
            'tracker_id' => $tracker,
        ]);

        //9:13
        $message->created_at = $tracker->start_time->addMinutes($tracker->check_in_frequency);
        $message->save();

        $this->assertEquals(\App\Tracker::STATUS_ORANGE, $tracker->status);
    }

    /**
     * @return void
     */
    public function test_tracker_status_comes_red_when_message_outside_of_a_frequency()
    {
        $tracker = factory(\App\Tracker::class)->create();
        $tracker->check_in_frequency = '10';

        //9:03pm
        $tracker->start_time = \Carbon\Carbon::now()->subMinutes($tracker->check_in_frequency * 4);
        $tracker->save();

        $message = factory(\App\Message::class)->create([
            'tracker_id' => $tracker,
        ]);

        //9:13
        $message->created_at = $tracker->start_time->addMinutes($tracker->check_in_frequency);
        $message->save();

        $this->assertEquals(\App\Tracker::STATUS_RED, $tracker->status);
    }
}
