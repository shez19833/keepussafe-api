<?php

use App\User;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UsersFriendsTest extends TestCase
{
    use DatabaseTransactions;

    private function sanitiseItems($items)
    {
        $data = [];

        foreach ($items as $item)
            $data[] = $this->sanitiseItem($item);

        return $data;
    }

    private function sanitiseItem($item)
    {
        return [
            'id' => (int) $item->id,
            'name' => (string) $item->name,
            'email' => (string) $item->email,
        ];
    }


    /**
     * @return void
     */
    public function test_i_can_get_all_friends_of_a_user()
    {
        $user = User::with('friends')->first();

        $this->actingAs($user)->json('GET', sprintf('users/me/friends'));

        $this->seeJsonContains(['data' => $this->sanitiseItems($user->friends)]);
    }

    /**
     * @return void
     */
    public function test_i_can_create_a_friend_for_a_user()
    {
        $user = User::first();
        $friend = factory(User::class)->make();

        $this->actingAs($user)->json('POST', sprintf('users/me/friends'), [
            'first_name' => $friend->first_name,
            'last_name' => $friend->last_name,
            'email' => $friend->email,
        ]);

        $friend = User::whereEmail($friend->email)->first();

        $this->seeInDatabase('users_friends', [
            'user_id' => $user->id,
            'friend_id' => $friend->id,
        ]);
    }

    /**
     * @return void
     */
    public function test_i_can_delete_a_friend_for_a_user()
    {
        $user = User::with('friends')->first();
        $friend = $user->friends->first();

        $this->actingAs($user)->json('DELETE', sprintf('users/me/friends/%d', $friend->id));

        $this->notSeeInDatabase('users_friends', [
            'user_id' => $user->id,
            'friend_id' => $friend->id,
        ]);
    }
}
