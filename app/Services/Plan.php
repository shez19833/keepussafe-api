<?php

namespace App\Services;

use Stripe\Plan as StripePlan;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Plan {

    public static function all()
    {
        // TODO cache it
        return StripePlan::all()->data;
    }

    public static function isValid($id)
    {
        $plans = self::all();

        foreach ( $plans as $plan )
        {
            if ( $plan->id === $id )
                return true;
        }

        return false;
    }

    public static function getName($id)
    {
        foreach ( self::all() as $plan )
        {
            if ( $plan->id === $id )
                return $plan->name;
        }

        throw new NotFoundHttpException;
    }

    public static function getIds()
    {
        $ids = [];
        foreach ( self::all() as $plan )
            $ids[] = $plan->id;

        return $ids;
    }

}