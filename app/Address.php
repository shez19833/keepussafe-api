<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 */
class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'line_1',
        'line_2',
        'city',
        'state',
        'postcode',
        'country_id',
        'latitude',
        'longitude',
    ];

    public $timestamps = false;


    // Accessors & Mutators

    // Scopes

    // Relations

    public function trackers()
    {
        return $this->hasMany(Tracker::class);
    }
}
