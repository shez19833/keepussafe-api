<?php

namespace App\Events;

class TrackerAlert extends Event
{
    public $tracker;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($tracker)
    {
        $this->tracker = $tracker;
    }
}
