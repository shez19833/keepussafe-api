<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Message extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'text' => (string) $this->text,
            'timestamp' => (string) $this->created_at,
        ];
    }
}
