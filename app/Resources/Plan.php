<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Plan extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int) $this->name,
            'name' => (string) $this->name,
            'amount' => (int) $this->amount,
            'frequency' => (int) $this->interval,
        ];
    }
}
