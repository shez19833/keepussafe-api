<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Address extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'line_1' => (string) $this->line_1,
            'line_2' => (string) $this->line_2,
            'city' => (string) $this->city,
            'state' => (string) $this->state,
            'postcode' => (string) $this->postcode,
            'country_id' => (string) $this->country_id,

            'latitude' => (double) $this->latitude,
            'longitude' => (double) $this->longitude,
        ];
    }
}
