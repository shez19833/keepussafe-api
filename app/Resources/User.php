<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int) $this->id,
            'first_name' => (string) $this->first_name,
            'last_name' => (string) $this->last_name,
            'email' => (string) $this->email,
            'secret_word' => (string) $this->secret_word,
            'secret_response' => (string)  $this->secret_response,
            'timezone' => (string)  $this->timezone,
        ];
    }
}