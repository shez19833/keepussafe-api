<?php

namespace App\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Tracker extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'location' =>  new Address($this->location),
//            'address' => (array) new Address($this->whenLoaded('address')),
            'start_time' => (string) $this->start_time,
            'end_time' => (string) $this->end_time,
            'check_in_frequency' => (int) $this->check_in_frequency,
            'share_id' => (string) $this->share_id,
            'status' => (string) $this->status,
        ];
    }
}
