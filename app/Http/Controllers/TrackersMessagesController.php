<?php

namespace App\Http\Controllers;

use App\{
    Events\TrackerAlert, Message, Tracker, Resources\Message as MessageResource
};
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;


class TrackersMessagesController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * @return mixed
     */
    public function index($trackerId)
    {
        // TODO make sure only authorized trackers owner can get messages

        $tracker = Tracker::with('messages')->findOrFail($trackerId);

        return MessageResource::collection($tracker->messages);
    }


    /**
     * @param Request $request
     * @param int $trackerId
     * @return JsonResponse
     */
    public function store(Request $request, int $trackerId) : JsonResponse
    {
        $tracker = Tracker::with('user')->findOrFail($trackerId);

        // TODO can only store message if u own the tracker

        Message::create([
           'tracker_id' => $tracker->id,
           'text' => $request->text,
        ]);

        if ( str_contains($request->text, $tracker->user->secret_word) )
        {
            event(new TrackerAlert($tracker));
            $reply = $tracker->user->secret_response;
        }
        else
        {
            $reply = 'Got it';
        }


        // TODO generate random real sentence if no library then repeat a random from array

        return response()->json(['data' => ['reply' => $reply] ], 201);
    }
}
