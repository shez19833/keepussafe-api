<?php

namespace App\Http\Controllers;

use App\Services\Plan;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;


class SubscriptionController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    private function doValidation($request)
    {
        $plans = implode(',', Plan::getIds()) ;

        $this->validate($request, [
            'token' => 'required|min:10',
            'plan' => 'required|in:'.$plans,
        ]);
    }

    private function checkSubscription($request, $subscription)
    {
        $user = $request->user();

        if ( $subscription && $subscription->active() )
        {
            $this->validate($request, [
                'plan' => 'not_in:' . $subscription->stripe_id,
            ]);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request) : JsonResponse
    {
        $this->doValidation($request);

        $subscription = $request->user()->subscriptions()->first();

        $this->checkSubscription($request, $subscription);

        try
        {
            if ( $subscription && $subscription->active() )
                $request->user()->subscription($subscription->stripe_name)->swap($request->plan);
            else
                $request->user()->newSubscription(Plan::getName($request->plan), $request->plan)->create($request->token);
        }
        catch (\Exception $e)
        {
            abort(422, $e->getMessage());
        }

        return response()->json([], 201);
    }

    public function destroy()
    {
        $user = \Auth::user();

        $subscription = $user->subscriptions()->first();

        try
        {
            $user->subscription($subscription->name)->cancel();
        }
        catch (\Exception $e)
        {
            abort(422, $e->getMessage());
        }

        return response()->json([], 200);
    }
}
