<?php

namespace App\Http\Controllers;

use App\{
    Address, Tracker, User,
    Resources\Tracker as TrackerResource
};
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;


// TODO tracker type 2 from address to end location
class UsersTrackersController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $user = User::with('trackers.location')->find(\Auth::id());

        return TrackerResource::collection($user->trackers);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show(int $id)
    {
        // TODO check if tracker belongs to the user

        $tracker = Tracker::with('location')->find($id);

        return new TrackerResource($tracker);
    }

    // TODO VALIDATION

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request) : JsonResponse
    {
        $user = User::findOrFail(\Auth::id());

        // TODO can only create your own tracker

        $location = Address::create([
            'latitude' => $request->input('latitude', null),
            'longitude' => $request->input('longitude', null),
        ]);

        $data = [
            'start_time' => $request->input('start_time', null),
            'end_time' => $request->input('end_time', null),
            'check_in_frequency' => $request->input('check_in_frequency', null),
            'location_id' => $location->id,
            'user_id' => $user->id,
        ];

        Tracker::create($data);

        return response()->json([], 201);
    }

    /**
     * @param Request $request
     * @param int $userId
     * @return JsonResponse
     */
    public function update(Request $request,int $id) : JsonResponse
    {
        $user = User::findOrFail(\Auth::id());
        $tracker = Tracker::with('location')->findOrFail($id);

        // TODO can only update your own tracker

        $location = Address::firstOrCreate([
            'latitude' => $request->input('latitude', null),
            'longitude' => $request->input('longitude', null),
        ]);

        $tracker->start_time = $request->input('start_time', (string) $tracker->start_time);
        $tracker->end_time = $request->input('end_time', (string) $tracker->end_time);
        $tracker->check_in_frequency = $request->input('check_in_frequency', $tracker->check_in_frequency);
        $tracker->location_id = $location->id;
        $tracker->save();

        return response()->json([], 201);
    }

    // TODO add in status ie warning etc after doing messages

    public function status($shareId)
    {
        $tracker = Tracker::with('location')->whereShareId($shareId)->firstOrFail();

        return new TrackerResource($tracker);
    }
}
