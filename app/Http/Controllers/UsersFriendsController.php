<?php

namespace App\Http\Controllers;

use App\User;
use App\Resources\UserFriend as UserFriendsResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UsersFriendsController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $user = User::with('friends')->find(\Auth::id());

        return UserFriendsResource::collection($user->friends);
    }

    // TODO VALIDATION

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request) : JsonResponse
    {
        $user = User::findOrFail(\Auth::id());

        $friend = User::create([
           'first_name' => $request->first_name,
           'last_name' => $request->last_name,
           'email' => $request->email,
        ]);

        $user->friends()->attach($friend->id);

        return response()->json([], 201);
    }

    public function delete(int $id) : JsonResponse
    {
        $user = User::findOrFail(\Auth::id());

        $user->friends()->detach($id);

        return response()->json([], 201);
    }
}
