<?php

namespace App\Http\Controllers;

use App\User;
use App\Services\Plan as PlanService;


class PlansController extends Controller
{
    /**
     * @return UserResource
     */
    public function index()
    {
        $plans = [];

        foreach (PlanService::all() as $plan)
        {
            $plans[] = [
                'id' => (int) $plan->name,
                'name' => (string) $plan->name,
                'amount' => (int) $plan->amount,
                'frequency' => (int) $plan->interval,
            ];
        }

        return response()->json(['data' => $plans]);
    }
}
