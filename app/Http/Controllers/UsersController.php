<?php

namespace App\Http\Controllers;

use App\User;
use App\Resources\User as UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * @return UserResource
     */
    public function show() : UserResource
    {
        return new UserResource(User::findOrFail(\Auth::id()));
    }

    // TODO VALIDATION

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request) : JsonResponse
    {
        $user = User::create([
           'email' => $request->email,
           'password' => app('hash')->make($request->password),
        ]);

        if ( $user )
            return response()->json([], 201);
        else
            return response()->json([], 422);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request) : JsonResponse
    {
        $user = User::where([
            'email' => $request->email,
        ])->first();

        if ( app('hash')->check($request->password, $user->password) )
            return response()->json(['data' => ['api_token' => $user->api_token]]);

        abort(503);
    }

    // TODO VALIDATION

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request) : JsonResponse
    {
        $user = User::findOrFail(\Auth::id());

        $user->first_name = $request->input('first_name', $user->first_name);
        $user->last_name = $request->input('last_name', $user->last_name);
        $user->secret_word = $request->input('secret_word', $user->secret_word);
        $user->secret_response = $request->input('secret_response', $user->secret_response);
        $user->timezone = $request->input('timezone', $user->timezone);

        $user->save();

        return response()->json([], 201);
    }
}
