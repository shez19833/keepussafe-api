<?php

namespace App;

use Laravel\Cashier\Billable;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * @property \Carbon\Carbon $deleted_at
 * @property mixed $friends
 * @property string $name
 * @property mixed $trackers
 */
class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, SoftDeletes, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'first_name',
        'last_name',
        'email',
        'password',
        'password_reset_token',
        'email_token',
        'email_verified',
        'secret_word',
        'secret_response',
        'timezone',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    // Accessors & Mutators
    /**
     * @return string
     */
    public function getNameAttribute() : string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    // Scopes

    // Relations

    public function friends()
    {
        return $this->belongsToMany(User::class, 'users_friends', 'user_id', 'friend_id');
    }

    public function trackers()
    {
        return $this->hasMany(Tracker::class)->orderBy('id', 'desc');
    }
}
