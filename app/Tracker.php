<?php

namespace App;

use Illuminate\Database\Eloquent\{
    Model, SoftDeletes
};


/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $deleted_at
 * @property mixed $location
 * @property mixed $address
 * @property \Carbon\Carbon $end_time
 * @property \Carbon\Carbon $start_time
 */
class Tracker extends Model
{
    use SoftDeletes;

    const STATUS_GREEN = 'Green';
    const STATUS_ORANGE = 'Orange';
    const STATUS_RED = 'Red';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'location_id',
        'address_id',
        'start_time',
        'end_time',
        'check_in_frequency',
    ];

    public $timestamps = false;

    public $dates = [
        'start_time',
        'end_time',
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->share_id = base64_encode(str_random(100));
        });
    }

    // Accessors & Mutators

    public function getStatusAttribute()
    {
        $message = $this->messages->last();

        $currentTime =  \Carbon\Carbon::now();
        $messageTime = isset( $message ) ? $message->created_at : $this->start_time;
        $difference = $currentTime->diffInMinutes($messageTime);

        if ( $currentTime < $messageTime  )
            return 'Green';

        switch ($difference)
        {
            case $difference < (int) $this->check_in_frequency:
                return self::STATUS_GREEN; break;

            case $difference <= (int) $this->check_in_frequency * 2:
                return self::STATUS_ORANGE; break;

            default:
                return self::STATUS_RED; break;
        }
    }

    // Scopes

    // Relations

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function location()
    {
        return $this->belongsTo(Address::class, 'location_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
