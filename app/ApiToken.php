<?php

namespace App;

class ApiToken
{
    public static function createApiToken()
    {
        return base64_encode(str_random(100));
    }
}
