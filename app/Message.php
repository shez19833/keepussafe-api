<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tracker_id',
        'text',
    ];

    // Accessors & Mutators

    // Scopes

    // Relations

    public function tracker()
    {
        return $this->belongsTo(Tracker::class);
    }
}
