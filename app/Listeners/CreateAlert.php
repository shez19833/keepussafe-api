<?php

namespace App\Listeners;

use App\Events\TrackerAlert;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateAlert implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent  $event
     * @return void
     */
    public function handle(TrackerAlert $event)
    {
        // TODO
        // Email ME
        // Send me an alert (Phone)
        // Send email/alerts to friends
    }
}
